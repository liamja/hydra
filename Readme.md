KalixAudio Hydra
================

A simple 3-band crossover plugin.


References
----------

See [Documentation/]() for more information, including filter
design.

* [http://www.linkwitzlab.com/crossovers.htm]()
* [http://sound.westhost.com/project125.htm]()


License
-------

GPL-3.0

See [License.md]() for full license text.

Copyright (C) 2014 Liam Anderson / KalixAudio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/]().

Hydra uses the following 3rd-party libraries:

* DSP Filters - Vinnie Falco (Used under an MIT license.)
* JUCE - Julian Storer (Used under a GPL license.)
