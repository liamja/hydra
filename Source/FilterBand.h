/*
  ==============================================================================

    FilterBand.h
    Created: 5 Feb 2014 8:48:02pm
    Author:  Liam Anderson

  ==============================================================================
*/

#ifndef FILTERBAND_H_INCLUDED
#define FILTERBAND_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class FilterBand : public AudioProcessor
{
public:
    FilterBand();
    ~FilterBand();
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (FilterBand)
};


#endif  // FILTERBAND_H_INCLUDED
