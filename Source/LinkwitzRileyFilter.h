/*
  ==============================================================================

    LinkwitzRileyFilter.h
    Created: 5 Feb 2014 10:18:59pm
    Author:  Liam

  ==============================================================================
*/

#ifndef LINKWITZRILEYFILTER_H_INCLUDED
#define LINKWITZRILEYFILTER_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <DspFilters/Dsp.h>

//==============================================================================
/*
 */
class LinkwitzRileyFilter : public AudioProcessor
{
public:
    LinkwitzRileyFilter (int order, double frequency);
    ~LinkwitzRileyFilter();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock);
    void releaseResources();

    void processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages);

    //==============================================================================
    AudioProcessorEditor* createEditor();
    bool hasEditor() const;

    //==============================================================================
    const String getName() const;

    int getNumParameters();

    float getParameter (int index);
    void setParameter (int index, float newValue);

    const String getParameterName (int index);
    const String getParameterText (int index);

    const String getInputChannelName (int channelIndex) const;
    const String getOutputChannelName (int channelIndex) const;
    bool isInputChannelStereoPair (int index) const;
    bool isOutputChannelStereoPair (int index) const;

    bool acceptsMidi() const;
    bool producesMidi() const;
    bool silenceInProducesSilenceOut() const;
    double getTailLengthSeconds() const;

    //==============================================================================
    int getNumPrograms();
    int getCurrentProgram();
    void setCurrentProgram (int index);
    const String getProgramName (int index);
    void changeProgramName (int index, const String& newName);

    //==============================================================================
    void getStateInformation (MemoryBlock& destData);
    void setStateInformation (const void* data, int sizeInBytes);

    //==============================================================================
    void setOrder (int newOrder);
    void setOrderFromControlValue (double newOrder);
    double getOrderAsControlValue();
    String getOrderAsText();
    void setFrequency (double newFrequency);
    void setFrequencyFromControlValue (double newFrequency);
    double getFrequencyAsControlValue();
    String getFrequencyAsText();


private:
    int order;
    double frequency;
    
    static const int maxOrder = 4;
    static const int maxChannels = 2; // Stereo pair.
    
    AudioSampleBuffer lowPassBuffer, highPassBuffer;
    Array<AudioSampleBuffer> filterBuffers;

    Dsp::Params parameters;

    enum parameterId
    {
        idSampleRate,
        idOrder,
        idFrequency
    };

    typedef Dsp::Butterworth::Design::LowPass<maxOrder> LowPass;
    typedef Dsp::Butterworth::Design::HighPass<maxOrder> HighPass;

    Dsp::FilterDesign<LowPass, maxChannels, Dsp::DirectFormII> lowPass1;
    Dsp::FilterDesign<LowPass, maxChannels, Dsp::DirectFormII> lowPass2;
    Dsp::FilterDesign<HighPass, maxChannels, Dsp::DirectFormII> highPass1;
    Dsp::FilterDesign<HighPass, maxChannels, Dsp::DirectFormII> highPass2;

    JUCE_LEAK_DETECTOR (LinkwitzRileyFilter)
};

#endif  // LINKWITZRILEYFILTER_H_INCLUDED
