/*
  ==============================================================================

    LinkwitzRileyFilter.cpp
    Created: 5 Feb 2014 10:18:59pm
    Author:  Liam
 
  ------------------------------------------------------------------------------
 
    Takes a stereo pair input and outputs a stereo low-passed band and a stereo
    high-passed band about the specified frequency.
 
           -- low-pass band output
          /
    input -- high-pass band output
 
    @see https://en.wikipedia.org/wiki/Linkwitz-Riley_filter

  ==============================================================================
*/

#include "LinkwitzRileyFilter.h"

//==============================================================================
LinkwitzRileyFilter::LinkwitzRileyFilter (int order, double frequency)
: order (order),
  frequency (frequency),
  lowPassBuffer (JucePlugin_MaxNumInputChannels, getBlockSize()),
  highPassBuffer (JucePlugin_MaxNumInputChannels, getBlockSize())
{
    jassert (frequency > 0);
    jassert (0 < order < maxOrder);
    filterBuffers.add (lowPassBuffer);
    filterBuffers.add (highPassBuffer);
    
    parameters = lowPass1.Dsp::Filter::getDefaultParams();
    parameters[idOrder] = order;
    parameters[idFrequency] = frequency;
    lowPass1.setParams (parameters);
    lowPass2.setParams (parameters);
    highPass1.setParams (parameters);
    highPass2.setParams (parameters);
}

LinkwitzRileyFilter::~LinkwitzRileyFilter()
{
}

void LinkwitzRileyFilter::prepareToPlay (double sampleRate,
                                         int estimatedSamplesPerBlock)
{
    parameters = lowPass1.Dsp::Filter::getDefaultParams();
    parameters[idSampleRate] = sampleRate;
    parameters[idOrder] = order;
    parameters[idFrequency] = frequency;
    lowPass1.setParams (parameters);
    lowPass2.setParams (parameters);
    highPass1.setParams (parameters);
    highPass2.setParams (parameters);
    
    for (AudioSampleBuffer &filterBuffer : filterBuffers)
    {
        filterBuffer.setSize (getNumInputChannels(), estimatedSamplesPerBlock);
    }
}

void LinkwitzRileyFilter::processBlock (AudioSampleBuffer& buffer,
                                        MidiBuffer& midiMessages)
{
    for (AudioSampleBuffer &filterBuffer : filterBuffers)
    {
        for (int chan = 0; chan < getNumInputChannels(); chan++)
        {
            filterBuffer.copyFrom (chan, 0, buffer,
                                   chan, 0, buffer.getNumSamples());
        }
    }

    // Cascade filters to construct a Linkwitz-Riley filter.
    AudioSampleBuffer &lb = filterBuffers.getReference(0);
    AudioSampleBuffer &hb = filterBuffers.getReference(1);
    {
        lowPass1.process (lb.getNumSamples(),
                         lb.getArrayOfChannels());
        highPass1.process (hb.getNumSamples(),
                          hb.getArrayOfChannels());
        lowPass2.process (lb.getNumSamples(),
                         lb.getArrayOfChannels());
        highPass2.process (hb.getNumSamples(),
                          hb.getArrayOfChannels());
    }

    int currentFilterBuffer = 0;
    for (AudioSampleBuffer &filterBuffer : filterBuffers)
    {
        for (int chan = 0; chan < getNumInputChannels(); chan++)
        {
            buffer.copyFrom (chan + currentFilterBuffer, 0, filterBuffer,
                             chan, 0, filterBuffer.getNumSamples());
        }
        currentFilterBuffer += 2;
    }
}

const String LinkwitzRileyFilter::getName() const
{
    return JucePlugin_Name;
}

int LinkwitzRileyFilter::getNumParameters()
{
	return 0;
}

float LinkwitzRileyFilter::getParameter (int index)
{
    return 0.0f;
}

void LinkwitzRileyFilter::setParameter (int index, float newValue)
{
}

const String LinkwitzRileyFilter::getParameterName (int index)
{
    return String::empty;
}

const String LinkwitzRileyFilter::getParameterText (int index)
{
    return String::empty;
}

const String LinkwitzRileyFilter::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String LinkwitzRileyFilter::getOutputChannelName (int channelIndex) const
{
    return String::empty;
}

bool LinkwitzRileyFilter::isInputChannelStereoPair (int index) const
{
    return true;
}

bool LinkwitzRileyFilter::isOutputChannelStereoPair (int index) const
{
    return true;
}

bool LinkwitzRileyFilter::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool LinkwitzRileyFilter::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool LinkwitzRileyFilter::silenceInProducesSilenceOut() const
{
    return false;
}

double LinkwitzRileyFilter::getTailLengthSeconds() const
{
    return 0.0;
}

int LinkwitzRileyFilter::getNumPrograms()
{
    return 0;
}

int LinkwitzRileyFilter::getCurrentProgram()
{
    return 0;
}

void LinkwitzRileyFilter::setCurrentProgram (int index)
{
}

const String LinkwitzRileyFilter::getProgramName (int index)
{
    return String::empty;
}

void LinkwitzRileyFilter::changeProgramName (int index, const String& newName)
{
}

void LinkwitzRileyFilter::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

//==============================================================================
bool LinkwitzRileyFilter::hasEditor() const
{
    return false; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* LinkwitzRileyFilter::createEditor()
{
    return nullptr;
}

//==============================================================================
void LinkwitzRileyFilter::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void LinkwitzRileyFilter::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
void LinkwitzRileyFilter::setOrderFromControlValue (double newOrder)
{
    jassert (0 <= newOrder <= 1);
    
    if (0 <= newOrder <= 1 && newOrder != order) {
        const Dsp::ParamInfo &o = lowPass1.getParamInfo (idOrder);
        order = o.toNativeValue (newOrder);
        lowPass1.setParam (idOrder, o.toNativeValue (newOrder));
        lowPass2.setParam (idOrder, o.toNativeValue (newOrder));
        highPass1.setParam (idOrder, o.toNativeValue (newOrder));
        highPass2.setParam (idOrder, o.toNativeValue (newOrder));
    }
}

double LinkwitzRileyFilter::getOrderAsControlValue()
{
    const Dsp::ParamInfo &o = lowPass1.getParamInfo (idOrder);
    return o.toControlValue (lowPass1.getParam (idOrder));
}

String LinkwitzRileyFilter::getOrderAsText()
{
    const Dsp::ParamInfo &o = lowPass1.getParamInfo (idOrder);
    return o.toString (lowPass1.getParam (idOrder));
}

void LinkwitzRileyFilter::setFrequency (double newFrequency)
{
    if (newFrequency != frequency) {
        frequency = parameters[idFrequency] = newFrequency;
        lowPass1.setParam (idFrequency, newFrequency);
        lowPass2.setParam (idFrequency, newFrequency);
        highPass1.setParam (idFrequency, newFrequency);
        highPass2.setParam (idFrequency, newFrequency);
    }
}

void LinkwitzRileyFilter::setFrequencyFromControlValue (double newFrequency)
{
    jassert (0 <= newFrequency <= 1);
    
    if (0 <= newFrequency <= 1 && newFrequency != frequency) {
        const Dsp::ParamInfo &freq = lowPass1.getParamInfo (idFrequency);
        frequency = freq.toNativeValue (newFrequency);
        lowPass1.setParam (idFrequency,
                               freq.toNativeValue (newFrequency));
        lowPass2.setParam (idFrequency,
                               freq.toNativeValue (newFrequency));
        highPass1.setParam (idFrequency,
                                freq.toNativeValue (newFrequency));
        highPass2.setParam (idFrequency,
                                freq.toNativeValue (newFrequency));
    }
}

double LinkwitzRileyFilter::getFrequencyAsControlValue()
{
    const Dsp::ParamInfo &freq = lowPass1.getParamInfo (idFrequency);
    return freq.toControlValue (lowPass1.getParam (idFrequency));
}

String LinkwitzRileyFilter::getFrequencyAsText()
{
    return String (lowPass1.getParam (idFrequency));
}
