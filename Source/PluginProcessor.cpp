/*
 ==============================================================================

 This file was auto-generated!

 It contains the basic startup code for a Juce application.

 ==============================================================================
 */

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
HydraAudioProcessor::HydraAudioProcessor()
: filterBuffer (JucePlugin_MaxNumInputChannels, getBlockSize())
{
    // Initialise with a 3-band crossover.
    // We can add or remove bands later when necessary.
    double freq[3] = {0, 200, 5000};
    for (int i = 1; i < numOfBands; ++i)
    {
        linkwitzRileyFilters.add (new LinkwitzRileyFilter (4, freq[i]));
    }
}

HydraAudioProcessor::~HydraAudioProcessor()
{
}

//==============================================================================
const String HydraAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

int HydraAudioProcessor::getNumParameters()
{
	return numOfUIParameters;
}

float HydraAudioProcessor::getParameter (int index)
{
    switch (index)
    {
        case lowFrequency:
            return linkwitzRileyFilters.getFirst()->getFrequencyAsControlValue();
        case highFrequency:
            return linkwitzRileyFilters.getLast()->getFrequencyAsControlValue();
        case filterOrder:
            return linkwitzRileyFilters.getFirst()->getOrderAsControlValue();
        default:
            return 0.0f;
    }
}

void HydraAudioProcessor::setParameter (int index, float newValue)
{
	switch (index)
    {
        case lowFrequency:
            linkwitzRileyFilters.getFirst()->setFrequencyFromControlValue (newValue);
            break;
        case highFrequency:
            linkwitzRileyFilters.getLast()->setFrequencyFromControlValue (newValue);
            break;
        case filterOrder:
            for (LinkwitzRileyFilter *filter : linkwitzRileyFilters)
            {
                filter->setOrderFromControlValue (newValue);
            }
            break;
        default:
            break;
	};
}

const String HydraAudioProcessor::getParameterName (int index)
{
    return UIParameter[index];
}

const String HydraAudioProcessor::getParameterText (int index)
{
    switch (index)
    {
        case lowFrequency:
            return String (linkwitzRileyFilters.getFirst()->getFrequencyAsText() + " Hz");
        case highFrequency:
            return String (linkwitzRileyFilters.getLast()->getFrequencyAsText() + " Hz");
        case filterOrder:
            return String (linkwitzRileyFilters.getFirst()->getOrderAsText());
        default:
            return String::empty;
	};
}

const String HydraAudioProcessor::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String HydraAudioProcessor::getOutputChannelName (int channelIndex) const
{
    switch (channelIndex)
    {
        case 0:
        case 1:
            return "Passthrough";
        case 2:
        case 3:
            return "Low Band";
        case 4:
        case 5:
            return "Middle Band";
        case 6:
        case 7:
            return "High Band";
        default:
            return String (channelIndex + 1);
	};
}

bool HydraAudioProcessor::isInputChannelStereoPair (int index) const
{
    return true;
}

bool HydraAudioProcessor::isOutputChannelStereoPair (int index) const
{
    return true;
}

bool HydraAudioProcessor::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool HydraAudioProcessor::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool HydraAudioProcessor::silenceInProducesSilenceOut() const
{
    return false;
}

double HydraAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int HydraAudioProcessor::getNumPrograms()
{
    return 0;
}

int HydraAudioProcessor::getCurrentProgram()
{
    return 0;
}

void HydraAudioProcessor::setCurrentProgram (int index)
{
}

const String HydraAudioProcessor::getProgramName (int index)
{
    return String::empty;
}

void HydraAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void HydraAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    for (LinkwitzRileyFilter *filter : linkwitzRileyFilters)
    {
        filter->setPlayConfigDetails (getNumInputChannels(),
                                     getNumInputChannels() * 2,
                                     sampleRate,
                                     samplesPerBlock);
        filter->prepareToPlay (sampleRate, samplesPerBlock);
    }

    filterBuffer.setSize (getNumOutputChannels(), samplesPerBlock);
}

void HydraAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void HydraAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...

    // In case we have more outputs than inputs, we'll clear any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = getNumInputChannels(); i < getNumOutputChannels(); ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
        filterBuffer.clear (i, 0, filterBuffer.getNumSamples());
    }

    filterBuffer = buffer;
    
    int currentFilterBuffer = 0;
    for (LinkwitzRileyFilter *filter : linkwitzRileyFilters)
    {
        currentFilterBuffer += numOfChannelsInGroup;
        filter->processBlock (filterBuffer, midiMessages);

        for (int chan = 0; chan < getNumInputChannels(); chan++)
        {
            buffer.copyFrom (chan + currentFilterBuffer, 0, filterBuffer,
                             chan, 0, filterBuffer.getNumSamples());

            // If this is the last filter, we need the high-passed output rather
            // than the low-passed.
            if (filter == linkwitzRileyFilters.getLast())
            {
                buffer.copyFrom (chan + numOfChannelsInGroup + currentFilterBuffer,
                                 0, filterBuffer,
                                 chan + numOfChannelsInGroup,
                                 0, filterBuffer.getNumSamples());
            }
            else
            {
                // Copy the high-passed output back into the input
                // for the next filter.
                filterBuffer.copyFrom (chan, 0, filterBuffer,
                                       chan + numOfChannelsInGroup, 0, filterBuffer.getNumSamples());
            }
        }
    }
}

//==============================================================================
bool HydraAudioProcessor::hasEditor() const
{
    return false; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* HydraAudioProcessor::createEditor()
{
    return new HydraAudioProcessorEditor (this);
}

//==============================================================================
void HydraAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.

    XmlElement xml (String (JucePlugin_Name));

    // add some attributes to it..
    for (int i = 0; i < numOfUIParameters; i++) {
        xml.setAttribute (UIParameter[i].replaceCharacters (" ", "_"),
                          getParameter (i));
    }

    copyXmlToBinary (xml, destData);
}

void HydraAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.

    ScopedPointer<XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));

    if (xmlState != nullptr)
    {
        if (xmlState->hasTagName (String (JucePlugin_Name)))
        {
            for (int i = 0; i < numOfUIParameters; i++) {
                setParameter (i, xmlState->getDoubleAttribute (UIParameter[i].replaceCharacters ("_", " ")));
            }
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new HydraAudioProcessor();
}
